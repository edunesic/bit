const   mongoose            = require('mongoose')
    ,   Planet              = mongoose.model('Planet')
    ,   fetch               = require("node-fetch")
    ,   { validate }        = require('../handlers/planetRequestValidation');

const   UNPROCESSABLE_ENTITY_RESPONSE   = 422
    ,   CREATED_RESPONSE                = 201
    ,   NOT_MODIFIED                    = 304
    ,   BAD_REQUEST                     = 400
    ,   NOT_FOUND                       = 404
    ,   STAR_WARS_API                   = 'https://swapi.co/api';

exports.add = async (req, res) => {
    const err = validate(req.body)
    
    if(err) {
        res.status(BAD_REQUEST).send(err.details.message);
        return;
    }

    const existingPlanet = await Planet.findOne({
        nome: req.body.nome
    })

    if(existingPlanet) {
        res.status(NOT_MODIFIED).send();
        return;       
    }

    const response  = await fetch(`${STAR_WARS_API}/planets?search=${req.body.nome}`);
    const json      = await response.json();
    const apiPlanet = json.results.find( planet => planet.name === req.body.nome)
    
    if (!apiPlanet) {
        res.status(UNPROCESSABLE_ENTITY_RESPONSE).send({ message: 'could not find the current planet'});
        return;
    }

    req.body.apiId              = apiPlanet.url.replace(/[\D:.\/]/g,"")
    req.body.aparicoesFilmes    = apiPlanet.films.length;

    const planet = new Planet(req.body);
    await planet.save();
    res.status(CREATED_RESPONSE).send({ message: 'new planet created'});
}

exports.list = async (req, res) => {
    const planets = await Planet.find({}, 
        {
            '_id': false,
            "__v": false
        })
    res.json(planets);
}

exports.findById = async (req, res) => {
    const planet = await Planet.findOne({
        apiId: req.params.id
    },
    {
        '_id': false,
        "__v": false
    })

    if(!planet) {
        res.status(NOT_FOUND)
        res.json({ message: "Planet not found"});
        return;
    }
    
    res.json(planet);
}

exports.findByName = async (req, res) => {
    const planet = await Planet.findOne({
        nome: req.params.name
    },
    {
        '_id': false,
        "__v": false
    })

    if(!planet) {
        res.status(NOT_FOUND)
        res.json({ message: "Planet not found"});
        return;
    }

    res.json(planet);
}

exports.delete = async (req, res) => {
    const planet = await Planet.deleteOne({
        nome: req.params.name
    })

    if(!planet.n) {
        res.status(NOT_FOUND)
        res.json({ message: "Planet not found"});
        return;
    }  

    res.json({message: `Planet ${req.params.name} sucessfully deleted!`});
}