const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const planetSchema = new mongoose.Schema({
    apiId: {
        type: Number,
        required: 'Must get this number from API'
    },
    aparicoesFilmes: {
        type: Number,
        required: "Must have appearances number"
    },
    nome: {
        type: String,
        required: 'You must have a text',
    },
    terreno: {
        type: String,
        required: 'You must have a text',
    },
    clima: {
        type: String,
        required: 'You must have a text',
    }
})

module.exports = mongoose.model('Planet', planetSchema)