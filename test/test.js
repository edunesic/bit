const chai = require('chai');
const supertest = require('supertest');
const expect = chai.expect;

const tatooine = {
    id: 1,
    nome: 'Tatooine',
    terreno: 'Areia',
    clima: 'Quente'
}

const yavinIV = {
    id: 3,
    nome: 'Yavin IV',
    terreno: 'Floresta',
    clima: 'Frio'
}

const request = supertest('http://localhost:3000/');

describe('Tests with empty database', () => {

    it('list', (done) => {
        request
            .get('planet/list')
            .expect(200)                        
            .end( (err, res) => {
                expect(res.body).to.have.lengthOf(0);
                done()
            })
    });
    it('find by name', (done) => {
        request
            .get('planet/name/Tatooine')
            .expect(404)            
            .end((err, res) => {
                expect(res.body.message).to.eql("Planet not found");
                done()
            })
    })
    it('find by id', (done) => {
        request
            .get('planet/id/1')
            .expect(404)            
            .end((err, res) => {
                expect(res.body.message).to.eql("Planet not found");
                done()
            })
    })
    it('delete', (done) => {
        request
            .delete('planet/1')
            .expect(404)            
            .end((err, res) => {
                expect(res.body.message).to.eql("Planet not found");
                done()
            })
    })
})

describe('Test adding values at database', () => {
    it('wrong url', (done) => {
        request
            .put('planets/wrong_url')
            .expect(404)
            .end(done)
    })
    it('planet add without parameters', (done) => {
        request
            .put('planet/add')
            .expect(400)
            .end(done)
    })
    it('planet add without nome', (done) => {
        request
            .put('planet/add')
            .send({ terreno: tatooine.terreno, clima: tatooine.clima })
            .expect(400)
            .end(done)
    })
    it('planet add without terreno', (done) => {
        request
            .put('planet/add')
            .send({ nome: tatooine.nome, clima: tatooine.clima })
            .expect(400)
            .end(done)
    })
    it('planet add without clima', (done) => {
        request
            .put('planet/add')
            .send({ nome: tatooine.nome, terreno: tatooine.terreno})
            .expect(400)
            .end(done)
    })
    it('planet add Tatooine', (done) => {
        request
            .put('planet/add')
            .send(tatooine)
            .expect(201)
            .end((err, res) => {
                expect(res.body.message).to.eql('new planet created');
                done();
            })
    })
    it('planet add Tatooine again', (done) => {
        request
            .put('planet/add')
            .send(tatooine)
            .expect(304)
            .end(done)
    })
    it('try to add planet with incomplete name(fullName: Yavin IV)', (done) => {
        request
            .put('planet/add')
            .send({ nome: 'Yavin', terreno: yavinIV.terreno, clima: yavinIV.clima})
            .expect(422)
            .end((err, res) => {
                expect(res.body.message).to.eql('could not find the current planet');
                done()
            })
    })
    it('planet add Yavin IV', (done) => {
        request
            .put('planet/add')
            .send(yavinIV)
            .expect(201)
            .end( (err, res) => {
                expect(res.body.message).to.eql('new planet created');
                done();
            })
    })
    it('list all', (done) => {
        request
            .get('planet/list')
            .expect(200)
            .then(res => {
                expect(res.body).to.have.lengthOf(2);
                expect(res.body[0]).to.have.property('nome');
                expect(res.body[0]).to.have.property('terreno');
                expect(res.body[0]).to.have.property('clima');
                done();
            })
    })
    it('get tatooine by name', done => {
        request
            .get(`planet/name/${tatooine.nome}`)
            .expect(200)
            .then(res => {
                expect(res.body.nome).to.eql(tatooine.nome);
                expect(res.body.terreno).to.eql(tatooine.terreno);
                expect(res.body.clima).to.eql(tatooine.clima); 
                expect(res.body).to.have.property('apiId');                               
                expect(res.body).to.have.property('aparicoesFilmes');
                done()                               
            })
    })
    it('get yavin by id', done => {
        request
            .get(`planet/id/${yavinIV.id}`)   
            .expect(200)    
            .then(res => {
                expect(res.body.nome).to.eql(yavinIV.nome);
                expect(res.body.terreno).to.eql(yavinIV.terreno);
                expect(res.body.clima).to.eql(yavinIV.clima); 
                expect(res.body).to.have.property('apiId');
                expect(res.body).to.have.property('aparicoesFilmes');
                done();
            })
    })
    it('delete tatooine', (done) => {
        request
            .delete(`planet/${encodeURI(tatooine.nome)}`)
            .expect(200)  
            .then(res => {
                expect(res.body.message).to.eql(`Planet ${tatooine.nome} sucessfully deleted!`);
                done()
            })
    })
    it('delete Yavin IV', (done) => {
        request
            .delete(`planet/${encodeURI(yavinIV.nome)}`)
            .expect(200)  
            .then(res => {
                expect(res.body.message).to.eql(`Planet ${yavinIV.nome} sucessfully deleted!`);
                done()
            })
    })
})