const Joi = require('joi');

const planetRequestSchema = Joi.object().keys({
    nome    : Joi.string().required(),
    terreno : Joi.string().required(),
    clima   : Joi.string().required()
})

exports.validate = (body) => {
    return Joi.validate(body, planetRequestSchema, { allowUnknown : true }).error;
}