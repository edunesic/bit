const   express           = require('express')
    ,   router            = express.Router()
    ,   planetController  = require('../controllers/planetController')
    ,   { catchErrors }   = require('../handlers/errorHandlers')
    ,   mapping           = '/planet';

router.put(`${mapping}/add`             , catchErrors(planetController.add));
router.get(`${mapping}/list`            , catchErrors(planetController.list));
router.get(`${mapping}/id/:id`          , catchErrors(planetController.findById));
router.get(`${mapping}/name/:name`      , catchErrors(planetController.findByName));
router.delete(`${mapping}/:name`        , catchErrors(planetController.delete));

module.exports = router;
