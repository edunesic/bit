const 	express       	= require('express')
	,			bodyParser    	= require('body-parser')
	,			routes        	= require('./routes/index')
	,			errorHandlers 	= require('./handlers/errorHandlers')
	,			app 						= express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', routes);
app.use(errorHandlers.notFound);

if (app.get('env') === 'development') {
  app.use(errorHandlers.developmentErrors);
}

app.use(errorHandlers.productionErrors);

module.exports = app;
